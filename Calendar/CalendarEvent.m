//
//  CalendarEvent.m
//  Calendar
//
//  Created by Maksim Rakhleev on 15.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "CalendarEvent.h"
#import <EventKit/EventKit.h>
#import <UIKit/UIKit.h>

@implementation CalendarEvent

+ (CalendarEvent *)sharedCalendarEvent {
    
    static CalendarEvent *calendarEvent;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        calendarEvent = [[CalendarEvent alloc]init];
        
    });
    return calendarEvent;
}

- (void)createEventTitle:(NSString *)title
                   notes:(NSString *)notes
               startDate:(NSDate *)startDate
                 endDate:(NSDate *)endDate
                location:(NSString *)location
                 success:(void (^)())success
                 failure:(void (^)(NSError *))failure {
    
    EKEventStore *store = [[EKEventStore alloc]init];
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                failure(error);
            });
            return;
        }
        
        if (granted) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                EKEvent *event = [self eventWithStore:store
                                                title:title
                                                notes:notes
                                            startDate:startDate
                                              endDate:endDate
                                             location:location
                                               allDay:YES
                                             calendar:[store defaultCalendarForNewEvents]];
                
                NSError *error = nil;
                [store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                
                if (error) {
                    
                    failure(error);
                } else {
                    success();
                }
            });
            
        } else {
            NSError *accessError =
            [NSError errorWithDomain:@"com.mobileJet.calendarEvent"
                                code:0
                            userInfo:[NSDictionary dictionaryWithObject:@"Denied access to your calendar" forKey:NSLocalizedDescriptionKey]];
            failure(accessError);
        }
    }];
}

- (EKEvent *)eventWithStore:(EKEventStore *)store
                            title:(NSString *)title
                            notes:(NSString *)notes
                        startDate:(NSDate *)startDate
                          endDate:(NSDate *)endDate
                         location:(NSString *)location
                           allDay:(BOOL)allDay
                         calendar:(EKCalendar *)calendar {
    
    EKEvent *event = [EKEvent eventWithEventStore:store];
    event.title = title;
    event.notes = notes;
    event.startDate = startDate;
    event.endDate = endDate;
    event.location = location;
    event.allDay = allDay;
    event.calendar = calendar;
    
    return event;
}


@end
