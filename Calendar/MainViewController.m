//
//  MainViewController.m
//  Calendar
//
//  Created by Maksim Rakhleev on 15.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "MainViewController.h"
#import "CalendarEvent.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (IBAction)actionCreateEvent:(UIButton *)sender {
    
    [[CalendarEvent sharedCalendarEvent]createEventTitle:@"Title"
                                                   notes:@"Notes"
                                               startDate:[NSDate date]
                                                 endDate:[NSDate dateWithTimeIntervalSinceNow:60 * 60 * 24]
                                                location:@"Minsk" success:^{
                                                    NSLog(@"SUCCESS");
                                                } failure:^(NSError *error) {
                                                    NSLog(@"ERROR. %@", error.localizedDescription);
                                                }];
}

@end
