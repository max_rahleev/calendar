//
//  CalendarEvent.h
//  Calendar
//
//  Created by Maksim Rakhleev on 15.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarEvent : NSObject

+ (CalendarEvent *)sharedCalendarEvent;

- (void)createEventTitle:(NSString *)title
                   notes:(NSString *)notes
               startDate:(NSDate *)startDate
                 endDate:(NSDate *)endDate
                location:(NSString *)location
                 success:(void(^)())success
                 failure:(void(^)(NSError *error))failure;

@end
